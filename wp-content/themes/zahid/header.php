<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
    <header class="header">
        <div class="container-fluid">
            <div class="row justify-content-between header-top">
                <div class="col d-flex align-items-center">
                    <p class="slog">АГРОТЕХ-ЗАХІД</p>
                </div>
                <div class="col-lg-4 col-1"></div>
                <div class="col header-links d-flex align-items-center justify-content-end">
                    <ul class="inline-list">
                        <li>
                            <a href="#">КОНТАКТИ</a>
                        </li>
                        <li>
                            <a href="#">КАТАЛОГ</a>
                        </li>
                        <li>
                            <a href="#">КОШИК</a>
                        </li>
                        <li>
                            <a href="#">МОВА</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="logo">
                <?php twentyfifteen_the_custom_logo(); ?>
            </div>
        </div>

        <div class="menu-wrapper">

            <div class="container sm-full">
                <div class="row search-wrapper">
                    <div class="col"></div>
                    <div class="col-4 search">
                        <form class="">
                            <input class="form-control mr-sm-2 input-search" type="search" placeholder="Пошук..." aria-label="Search">
                            <!--<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>-->
                        </form>
                    </div>
                </div>
                <div class="menu row">
                    <div class="col-7 env-block">
                        <h4>Обладнання</h4>

                        <ul class="inline-list">
                            <li>
                                <a href="#">Мінітрактори</a>
                            </li>
                            <li>
                                <a href="#">Мототрактори</a>
                            </li>
                            <li>
                                <a href="#">Причепи та навіси</a>
                            </li>
                            <li>
                                <a href="#">Генератори</a>
                            </li>
                            <li>
                                <a href="#">Всюдиходи</a>
                            </li>
                            <li>
                                <a href="#">Інше</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-5 search-block d-flex justify-content-end">
                        <ul class="inline-list">
                            <li>
                                <a href="#">Оплата і доставка</a>
                            </li>
                            <li>
                                <a href="#">Новини</a>
                            </li>
                            <li>
                                <a href="#">Представництва</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Top Navigation Menu -->
    <div class="mobile-top-nav">
        <a href="/" class="mobile-top-header">
            <img class="mobile-logo" src="images/logo.png"/>
        </a>
        <div id="top-nav-links">
            <a href="#news">News</a>
            <a href="#contact">Contact</a>
            <a href="#about">About</a>
        </div>
        <a href="javascript:void(0);" class="icon" onclick="myFunction()">
            <span class="navbar-toggler-icon"></span>
        </a>
    </div>
    <?php get_sidebar(); ?>

	<div id="content" class="site-content">
