<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

	</div><!-- .site-content -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <h4 class="h4"> Швидкі посилання </h4>
                    <ul class="footer-links">
                        <li>
                            <a href="#">Мінітрактори</a>
                        </li>
                        <li>
                            <a href="#">Мототрактори</a>
                        </li>
                        <li>
                            <a href="#">Причепи та навіси</a>
                        </li>
                        <li>
                            <a href="#">Генератори</a>
                        </li>
                        <li>
                            <a href="#">Всюдиходи</a>
                        </li>
                        <li>
                            <a href="#">Інше</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <h4 class="h4"> Звязок </h4>
                    <form class="contact-form">
                        <div class="form-group">
                            <input type="text" placeholder="Імя"/>
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="Електронна пошта"/>
                        </div>
                        <div class="form-group">
                            <textarea placeholder="Повідомлення"></textarea>
                        </div>
                    </form>
                </div>
                <div class="col-lg-4 col-sm-12">
                    <h4 class="h4"> Ми в соцмережах </h4>
                    <div class="row">
                        <div class="col-5">
                            <ul class="social inline-list">
                                <li>
                                    <a href="#">
                                        <i class="social-icon facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="social-icon twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="social-icon youtube"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="social-icon instagramm"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="social-icon linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="social-icon google"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p class="copyright">АГРОТЕХ-ЗАХІД. Підстримка сайту</p>
                </div>
                <div class="site-info">
                    <?php
                    /**
                     * Fires before the Twenty Fifteen footer text for footer customization.
                     *
                     * @since Twenty Fifteen 1.0
                     */
                    do_action( 'twentyfifteen_credits' );
                    ?>
                    <?php
                    if ( function_exists( 'the_privacy_policy_link' ) ) {
                        the_privacy_policy_link( '', '<span role="separator" aria-hidden="true"></span>' );
                    }
                    ?>
                    <a href="<?php echo esc_url( __( 'https://wordpress.org/', 'twentyfifteen' ) ); ?>" class="imprint">
                        <?php printf( __( 'Proudly powered by %s', 'twentyfifteen' ), 'WordPress' ); ?>
                    </a>
                </div><!-- .site-info -->
            </div>

        </div>

    </footer>
    <script>
        function myFunction() {
            var x = document.getElementById("top-nav-links");
            if (x.style.display === "block") {
                x.style.display = "none";
            } else {
                x.style.display = "block";
            }
        }
    </script>


</div><!-- .site -->

<?php wp_footer(); ?>

</body>
</html>
